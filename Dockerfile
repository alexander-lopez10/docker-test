FROM notarize/circleci:3.0.0

RUN apt-get update -y \
  && apt-get upgrade -y \
  && apt-get install -y unzip curl postgresql-client-11 \
  && mkdir /tmp/awscli-src && cd /tmp/awscli-src \
  && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.2.9.zip" -o "awscliv2.zip" \
  && unzip awscliv2.zip \
  && ./aws/install \
  && cd && rm -rf /tmp/awscli-src
